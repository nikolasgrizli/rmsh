import $ from 'jquery';
import 'tether';
import focusWithin from 'focus-within';

focusWithin(document, {
  attr: false,
  className: 'focus-within'
});

global.Tether = require('tether');
require('popper.js/dist/umd/popper');
require('bootstrap');

$(document).on('click','.social-absolute .share-close', function() {
  $(this).closest('.social-absolute').toggleClass('is-closed');
});

//table with sticky th

function tableHeaderWidth(){
  let $tableHeader = $('.table-area > table > *:first-child');
  let $tableWithHeader = $tableHeader.closest('table');
  if($tableWithHeader.length > 0) {
    $tableHeader.css('width', $tableWithHeader.width());
  }
}
$(window).on('resize', function() {
  tableHeaderWidth();
});

// polyfill for sticky blocks
let stickyElems = $('.table-area > table > *:first-child');
let Stickyfill = require('stickyfill');
var stickyfill = Stickyfill();
stickyfill.add(stickyElems);


// function checkStatusMenu() {
//   if($('[mob-menu-wrapper]').hasClass('is-open') && $('body > .wrapper').hasClass('menu-open')) {
//     $('body > .wrapper').addClass('menu-open');
//   } else {
//     $('body > .wrapper').removeClass('menu-open');
//   }
// }

//mobile menu
$(document).on('click', '.mob-calls__menu', function() {
  $(this).closest('[mob-menu-wrapper]').toggleClass('is-open');
  // checkStatusMenu(); 
});
$(document).on('click', function(e) {
  var menu = $('[mob-menu-wrapper]');
  if(!menu.is(e.target) && menu.has(e.target).length === 0 && menu.is('.is-open')) {
    menu.removeClass('is-open');
  }
  // checkStatusMenu();
});


$(document).ready(function() {
  $('.js-to').click(function(e) {
    e.preventDefault();
    let $target = $($(this).attr('href')) ? $($(this).attr('href')) : $($(this).attr('data-href'));
    if($target.length) {
      $('html, body').animate({
        scrollTop: $target.offset().top + 'px'
      }, {
        duration: 500,
        easing: 'swing'
      });
      return false;
    } else {
      console.log('No target. Target must have id '+ $target.text());

    }

  });
  tableHeaderWidth();
});
