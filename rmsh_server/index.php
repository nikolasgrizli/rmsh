<?php
/** Stage */
try {@include_once('_auth.php');} catch (\Exception $exc) {}
?>

<?php $versionNb = 1.2; ?>
<!doctype html>

<html lang="en">
    <head class='test'>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?php include_once __DIR__.'/views/index/meta-title.php'; ?></title>
        <meta name="description" content="<?php include_once __DIR__.'/views/index/meta-description.php'; ?>">
        <meta name="theme-color" content="#000000">
        <link rel="canonical" href="https://ruinmysearchhistory.com/">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <link rel="stylesheet" href="rgs.css?v=<?php echo $versionNb; ?>">
        <link rel="stylesheet" href="reset.css">
        <link href='style.css?v=<?php echo $versionNb; ?>' rel='stylesheet' type='text/css'>
        <script src="ruin.js?v=<?php echo $versionNb; ?>"></script>

        <meta property="og:title" content="<?php include_once __DIR__.'/views/index/meta-title.php'; ?>" />
        <meta property="og:site_name" content="RMSH"/>
        <meta property="og:url" content="https://ruinmysearchhistory.com/" />
        <meta property="og:description" content="<?php include_once __DIR__.'/views/index/meta-description.php'; ?>" />
        <meta property="og:image" content="https://ruinmysearchhistory.com/rmsh-facebook.png?" />
        <meta property="og:type" content="website" />


        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="<?php include_once __DIR__.'/views/index/meta-title.php'; ?>" />
        <meta name="twitter:description" content="<?php include_once __DIR__.'/views/index/meta-description.php'; ?>" />
        <meta name="twitter:image:src" content="https://ruinmysearchhistory.com/rmsh-twitter.png?s" />

        <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/manifest.json">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">

        <!-- OG/Twitter cards -->

        <!--[if lt IE 9]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127858911-1"></script>
        <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-127858911-1'); </script>
    </head>

    <body>

        <div class="section group" style="background:#f2f0f2; border:2px solid #e6e4e6;">
            <div class="col span_4_of_4">
                <h1 id="initial"><span id="queryText"><span style="color:#3f85f7">Ruin</span> <span style="color:#f13b37">My</span> <span style="color:#fcbb07">Search</span> <span style="color:#34a857">History</span></span><button id="magnifyingGlass"></button></h1>
            </div>
        </div>

        <div class="container">

            <div class="section group span_4_of_4">

                <div class="col span_2_of_4 text-left">
                    <header id="tagline" class="menu">
                        <?php include_once __DIR__.'/views/index/tagline.php'; ?>
                    </header>
                </div>

                <div class="col span_2_of_4 text-right">
                    <?php include_once __DIR__.'/views/index/main-menu.php'; ?>
                </div>

            </div>

            <div class="span_4_of_4">

                <?php include_once __DIR__.'/views/index/disclaimer.php'; ?>
                
                <div id="ruinContainer">

                    <div class="section group">
                        <div class="col span_4_of_4">
                            <div id="ruinProgressText"></div>
                        </div>
                    </div>

                    <div class="section group">
                        <div class="col span_1_of_4"></div>
                        <div class="col span_2_of_4">
                            <div id="ruinProgressBar"></div>
                        </div>
                        <div class="col span_1_of_4"></div>
                    </div>

                    <div class="section group">
                        <div class="col span_4_of_4">
                            <ul id="ruinSearchesCompleted"></ul>
                        </div>
                    </div>

                </div>

                <?php include_once __DIR__.'/views/index/advert.php'; ?>

            </div>

            <div class="span_4_of_4">

                <?php include_once __DIR__.'/views/index/resent-blog-posts.php'; ?>
                
            </div>

            <div class="section group">
                <div class="span_4_of_4 text-left">
                    <div id="email"><?php include_once __DIR__.'/views/index/email.php'; ?></div>
                </div>
            </div>
            
        </div>

        <script src="http://ip-api.com/json/?callback=userLocation&fields=country,regionName,city,status" async></script>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57e054601b137431"></script>
    </body>
</html>