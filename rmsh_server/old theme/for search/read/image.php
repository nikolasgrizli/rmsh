<?php
/*
/////////////////////////////////////////////////////
 Previous content is saved as a file 'image.php.bak'
/////////////////////////////////////////////////////
*/
// These pages should return status error 410 (not 404!) and just reach an error page (i.e. not show content)
header( "HTTP/1.1 410 Gone" );

exit();
